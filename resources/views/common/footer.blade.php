   <!-- Footer Area -->
                <footer class="footer-area d-flex align-items-center flex-wrap">
                    <!-- Copywrite Text -->
                    <div class="copywrite-text">
                        <p>Ecaps &copy; 2019 created by <a href="https://wrapbootstrap.com/user/DesigningWorld" target="_blank">Designing World</a></p>
                    </div>
                    <!-- Footer Nav -->
                    <ul class="footer-nav d-flex align-items-center">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Purchase</a></li>
                    </ul>
                </footer>