 <header class="top-header-area d-flex align-items-center justify-content-between">
                <div class="left-side-content-area d-flex align-items-center">
                    <!-- Mobile Logo -->
                    <div class="mobile-logo mr-3 mr-sm-4">
                        <a href="index.html"><img src="img/core-img/small-logo.png" alt="Mobile Logo"></a>
                    </div>

                    <!-- Triggers -->
                    <div class="ecaps-triggers mr-1 mr-sm-3">
                        <div class="menu-collasped" id="menuCollasped">
                            <i class="ti-align-right"></i>
                        </div>
                        <div class="mobile-menu-open" id="mobileMenuOpen">
                            <i class="ti-align-right"></i>
                        </div>
                    </div>

                    <!-- Left Side Nav -->
                    <ul class="left-side-navbar d-flex align-items-center">
                        <li class="nav-item dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></button>
                            <!-- Dropdown Menu -->
                            <div class="dropdown-menu megamenu-item" aria-labelledby="dropdownMenuButton">
                                <div class="d-flex flex-wrap">
                                    <ul>
                                        <li>
                                            <a class="nav-link" href="#">* Accordian</a>
                                            <a class="nav-link" href="#">* Account Settings</a>
                                            <a class="nav-link" href="#">* Board</a>
                                            <a class="nav-link" href="#">* Basic Form</a>
                                            <a class="nav-link" href="#">* Card</a>
                                            <a class="nav-link" href="#">* Call To Action</a>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li>
                                            <a class="nav-link" href="#">- Chat Box</a>
                                            <a class="nav-link" href="#">- Clipboard</a>
                                            <a class="nav-link" href="#">- Contact</a>
                                            <a class="nav-link" href="#">- Data Table</a>
                                            <a class="nav-link" href="#">- Edit Table</a>
                                            <a class="nav-link" href="#">- Upload</a>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li>
                                            <a class="nav-link" href="#"># Email View</a>
                                            <a class="nav-link" href="#"># Invoice</a>
                                            <a class="nav-link" href="#"># Loader</a>
                                            <a class="nav-link" href="#"># Profile</a>
                                            <a class="nav-link" href="#"># Progressbar</a>
                                            <a class="nav-link" href="#"># Shop</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown ml-1 ml-sm-3">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Apps <i class="fa fa-angle-down"></i></button>
                            <!-- Dropdown Menu -->
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton6">
                                <a href="#" class="dropdown-item">- Profile</a>
                                <a href="#" class="dropdown-item">- Messages</a>
                                <a href="#" class="dropdown-item">- Settings</a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="right-side-navbar d-flex align-items-center justify-content-end">
                    <!-- Mobile Trigger -->
                    <div class="right-side-trigger" id="rightSideTrigger">
                        <i class="ti-align-left"></i>
                    </div>

                    <!-- Top Bar Nav -->
                    <ul class="right-side-content d-flex align-items-center">
                        <li class="nav-item dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search"></i></button>
                            <div class="dropdown-menu dropdown-menu-right search-dropdown">
                                <!-- Top Search Bar -->
                                <div class="top-search-bar">
                                    <form action="#" method="get">
                                        <input type="search" name="search" class="from-control top-search mb-0" placeholder="Type your keywords...">
                                        <button type="submit"><i class="ti-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-world"></i></button>
                            <div class="dropdown-menu language-dropdown dropdown-menu-right">
                                <a href="#" class="dropdown-item"><img src="img/core-img/l1.jpg" alt=""> IND</a>
                                <a href="#" class="dropdown-item"><img src="img/core-img/l2.jpg" alt=""> LOP</a>
                                <a href="#" class="dropdown-item"><img src="img/core-img/l3.jpg" alt=""> KYI</a>
                                <a href="#" class="dropdown-item"><img src="img/core-img/l4.jpg" alt=""> RTY</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-email" aria-hidden="true"></i></button>

                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- Top Message Area -->
                                <div class="top-message-area">
                                    <!-- Heading -->
                                    <div class="top-message-heading">
                                        <div class="heading-title">
                                            <h6>Messages</h6>
                                        </div>
                                        <span>5 New</span>
                                    </div>
                                    <div class="message-box" id="messageBox">
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>6-hour video course on Angular</span>
                                                <span>3 min ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>Google Ads: You'll get a refund soon</span>
                                                <span>27 min ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>New Feature: HTTP Method Selection</span>
                                                <span>56 min ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>The Complete JavaScript Handbook</span>
                                                <span>1 hour ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>New comment: ecaps Template</span>
                                                <span>2 days ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>6-hour video course on Angular</span>
                                                <span>3 min ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>Google Ads: You'll get a refund soon</span>
                                                <span>27 min ago</span>
                                            </span>
                                        </a>
                                        <a href="#" class="dropdown-item">
                                            <i class="ti-email"></i>
                                            <span class="message-text">
                                                <span>New Feature: HTTP Method Selection</span>
                                                <span>56 min ago</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-bell" aria-hidden="true"></i> <span class="active-status"></span></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- Top Notifications Area -->
                                <div class="top-notifications-area">
                                    <!-- Heading -->
                                    <div class="notifications-heading">
                                        <div class="heading-title">
                                            <h6>Notifications</h6>
                                        </div>
                                        <span>5 New</span>
                                    </div>

                                    <div class="notifications-box" id="notificationsBox">
                                        <a href="#" class="dropdown-item"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-check"></i><span>Your commissions has been sent</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-heart bg-success"></i><span>You sold an item!</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-check"></i><span>Your commissions has been sent</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-heart bg-success"></i><span>You sold an item!</span></a>
                                        <a href="#" class="dropdown-item"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span></a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="img/member-img/team-2.jpg" alt=""></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- User Profile Area -->
                                <div class="user-profile-area">
                                    <div class="user-profile-heading">
                                        <!-- Thumb -->
                                        <div class="profile-thumbnail">
                                            <img src="img/member-img/team-2.jpg" alt="">
                                        </div>
                                        <!-- Profile Text -->
                                        <div class="profile-text">
                                            <h6>Ajoy Das</h6>
                                            <span>ajoydas@example.com</span>
                                        </div>
                                    </div>
                                    <a href="#" class="dropdown-item"><i class="ti-user text-default" aria-hidden="true"></i> My profile</a>
                                    <a href="#" class="dropdown-item"><i class="ti-email text-success" aria-hidden="true"></i> Messages</a>
                                    <a href="#" class="dropdown-item"><i class="ti-settings text-default" aria-hidden="true"></i> Account settings</a>
                                    <a href="#" class="dropdown-item"><i class="ti-heart text-purple" aria-hidden="true"></i> Support</a>
                                    <a href="login.html" class="dropdown-item"><i class="ti-unlink text-warning" aria-hidden="true"></i> Sign-out</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <button type="button" class="btn" id="quicksettingTrigger"><i class="ti-view-grid"></i></button>
                        </li>
                    </ul>
                </div>
            </header>