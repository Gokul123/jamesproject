@include('components/main-head')

<body>
    <!-- Preloader -->
    <div id="preloader"></div>

    <!-- Choose Layout -->
    

    <!-- Quick Settings Panel -->
    <div class="quick-settings-panel">
        <div id="quicksettingCloseIcon"><i class="ti-close"></i></div>
        <div id="quickSettingPanel">
            <div class="quick-setting-tab">
                <ul class="nav nav-tabs" id="quickSettingTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="todo-list-tab" data-toggle="tab" href="#to-do-list" role="tab" aria-controls="to-do-list" aria-selected="true">Todo List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Notifications</a>
                    </li>
                </ul>

                <div class="tab-content" id="quickSettingTabContent">
                    <div class="tab-pane fade show active" id="to-do-list" role="tabpanel" aria-labelledby="todo-list-tab">
                        <div class="widgets-todo-list-area">
                            <form id="form-add-todo" class="form-add-todo">
                                <input type="text" id="new-todo-item" class="new-todo-item" name="todo" placeholder="Add New">
                                <input type="submit" id="add-todo-item" class="add-todo-item" value="+">
                            </form>

                            <form id="form-todo-list">
                                <ul id="ecapsToDo-list" class="todo-list">
                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="test"><span></span></label>Go to Market
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Meeting with AD
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Check Mail
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Work for Theme
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Buy Some Vegetables
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Call CEO
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Create a Plugin
                                        <i class="todo-item-delete ti-close"></i></li>

                                    <li><label class="ckbox"><input type="checkbox" name="todo-item-done" class="todo-item-done" value="hello"><span></span></label>Fixed Template Issues
                                        <i class="todo-item-delete ti-close"></i></li>
                                </ul>
                            </form>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                        <div class="quick--setting--option">
                            <h6 class="mb-4">Quick Settings</h6>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch" tabindex="1">
                                <label for="on-off-switch">Quick View</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch2" tabindex="1">
                                <label for="on-off-switch2">Get Feedbacks</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch3" checked="" tabindex="1">
                                <label for="on-off-switch3">Get Notifications</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch4" tabindex="1">
                                <label for="on-off-switch4">Todo Lists</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch5" tabindex="1">
                                <label for="on-off-switch5">Top Header</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch6" checked="" tabindex="1">
                                <label for="on-off-switch6">Sidebar</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="color--setting--option mt-5">
                            <h6 class="mb-4">Color Options</h6>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch7" checked="" tabindex="1">
                                <label for="on-off-switch7">Background Image</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch8" tabindex="1">
                                <label for="on-off-switch8">Light Sidebar</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch9" tabindex="1">
                                <label for="on-off-switch9">Dark Topbar</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch10" checked="" tabindex="1">
                                <label for="on-off-switch10">Footer Color</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch11" tabindex="1">
                                <label for="on-off-switch11">Header Dark</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Setting -->
                            <div class="toggle-group mb-4">
                                <input type="checkbox" name="on-off-switch" id="on-off-switch12" tabindex="1">
                                <label for="on-off-switch12">Prelaoder Color</label>
                                <div class="onoffswitch" aria-hidden="true">
                                    <div class="onoffswitch-label">
                                        <div class="onoffswitch-inner"></div>
                                        <div class="onoffswitch-switch"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
                        <div class="notifications-box">
                            <a href="#" class="nav-link px-3"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-check"></i><span>Your commissions has been sent</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-heart bg-success"></i><span>You sold an item!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-check"></i><span>Your commissions has been sent</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-heart bg-success"></i><span>You sold an item!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-check"></i><span>Your commissions has been sent</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-heart bg-success"></i><span>You sold an item!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bell bg-danger"></i><span>Domain names expiring on Tuesday</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-check"></i><span>Your commissions has been sent</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-heart bg-success"></i><span>You sold an item!</span> <i class="fa fa-angle-right"></i></a>
                            <a href="#" class="nav-link px-3"><i class="ti-bolt bg-warning"></i><span>Security alert for your linked Google account</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
    <div class="ecaps-page-wrapper">
        @include('components/sidebar')

        <!-- Page Content -->
        <div class="ecaps-page-content">
            @include('components/header')

            <!-- Main Content Area -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="dashboard-header-title mb-3">
                                <h6 class="mb-0">Welcome back!</h6>
                                <p class="mb-0">Congratulations, You have sold 3 new items.</p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="dashboard-infor-mation d-flex flex-wrap align-items-center mb-3">
                                <div class="dashboard-clock">
                                    <div id="dashboardDate"></div>
                                    <ul class="d-flex align-items-center justify-content-end">
                                        <li id="hours"></li>
                                        <li>:</li>
                                        <li id="min"></li>
                                        <li>:</li>
                                        <li id="sec"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="card bg-boxshadow">
                                <div class="card-body px-3 py-2">
                                    <div class="user-important-data-info d-sm-flex align-items-center justify-content-between">
                                        <ul class="downloads--data-btn d-flex align-items-center justify-content-between mb-3 mb-sm-0">
                                            <li><a href="#" class="btn"><i class="ti-arrow-down"></i> Downloads</a></li>
                                            <li><a href="#" class="btn"><i class="ti-reload"></i> Updates</a></li>
                                            <li><a href="#" class="btn"><i class="ti-import"></i> Import</a></li>
                                        </ul>
                                        <ul class="sales-reports d-flex align-items-center justify-content-between">
                                            <li><span>Last Week</span> <span class="counter">500.68</span></li>
                                            <li><span>This Week</span> <span class="counter">438.12</span></li>
                                            <li><span>Balance</span> <span class="counter">693.87</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider-area style-2 p-3 bg-white">
                                <div class="d-flex flex-wrap justify-content-between align-items-center mb-15">
                                    <h5 class="card-title mb-0">Summary</h5>
                                    <a href="#" class="btn btn-rounded btn-primary"><i class="ti-calendar"></i> Dec</a>
                                </div>
                                <div class="widget-slider-2 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$969.63 <span>(Profit)</span></h6>
                                        <p class="mb-0">by last month</p>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$768.36 <span>(Invoiced)</span></h6>
                                        <p class="mb-0">by last month</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider-area style-2 p-3 bg-white">
                                <div class="d-flex flex-wrap justify-content-between align-items-center mb-15">
                                    <h5 class="card-title mb-0">Expenses</h5>
                                    <a href="#" class="btn btn-rounded btn-success"><i class="ti-calendar"></i> Jan</a>
                                </div>
                                <div class="widget-slider-2 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$969.63</h6>
                                        <p class="mb-0">Today</p>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$768.36</h6>
                                        <p class="mb-0">Last week</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider-area style-2 p-3 bg-white">
                                <div class="d-flex flex-wrap justify-content-between align-items-center mb-15">
                                    <h5 class="card-title mb-0">Sales</h5>
                                    <a href="#" class="btn btn-rounded btn-info"><i class="ti-calendar"></i> Feb</a>
                                </div>
                                <div class="widget-slider-2 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$969.63</h6>
                                        <p class="mb-0">Today</p>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$768.36</h6>
                                        <p class="mb-0">Last week</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider-area style-2 p-3 bg-white">
                                <div class="d-flex flex-wrap justify-content-between align-items-center mb-15">
                                    <h5 class="card-title mb-0">Cost</h5>
                                    <a href="#" class="btn btn-rounded btn-danger"><i class="ti-calendar"></i> Dec</a>
                                </div>
                                <div class="widget-slider-2 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$969.63</h6>
                                        <p class="mb-0">Today</p>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-content">
                                        <h6 class="mb-0">$768.36</h6>
                                        <p class="mb-0">Last week</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">User By Countries</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown881" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown881">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="map--body">
                                        <div id="world-map-markers" class="height-300"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="card bg-boxshadow mb-3">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">User Power &amp; Skill</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown8819" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown8819">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="echart-chart-area">
                                        <!-- Echart chart -->
                                        <div class="echart-chart">
                                            <div id="basic_filled_radar" class="height-400"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header border-0 bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Projects of the Month</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown52" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown52">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body pt-0">
                                    <div class="table-responsive">
                                        <table class="table border-0 mb-0 project-table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Assigned</th>
                                                    <th>Name</th>
                                                    <th>Priority</th>
                                                    <th>Budget</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-2.jpg" alt="user">
                                                    <td>
                                                        <h6>Zara</h6><span class="text-muted">Designer</span>
                                                    </td>
                                                    <td>Power Admin</td>
                                                    <td><span class="badge badge-outline-warning badge-pill">Low</span></td>
                                                    <td>$4.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-3.jpg" alt="user">
                                                    <td>
                                                        <h6>David</h6><span class="text-muted">Developer</span>
                                                    </td>
                                                    <td>Elite Admin</td>
                                                    <td><span class="badge badge-outline-success badge-pill">Heigh</span></td>
                                                    <td>$11.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-4.jpg" alt="user">
                                                    <td>
                                                        <h6>Lina</h6><span class="text-muted">Designer</span>
                                                    </td>
                                                    <td>E-caps Admin</td>
                                                    <td><span class="badge badge-outline-info badge-pill">Medium</span></td>
                                                    <td>$5.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-5.jpg" alt="user">
                                                    <td>
                                                        <h6>David</h6><span class="text-muted">Developer</span>
                                                    </td>
                                                    <td>Elite Admin</td>
                                                    <td><span class="badge badge-outline-warning badge-pill">Low</span></td>
                                                    <td>$4.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-6.jpg" alt="user">
                                                    <td>
                                                        <h6>Jhon</h6><span class="text-muted">Designer</span>
                                                    </td>
                                                    <td>Admetro Admin</td>
                                                    <td><span class="badge badge-outline-success badge-pill">Heigh</span></td>
                                                    <td>$9.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-1.jpg" alt="user">
                                                    <td>
                                                        <h6>Smith</h6><span class="text-muted">Designer</span>
                                                    </td>
                                                    <td>Admetro Admin</td>
                                                    <td><span class="badge badge-outline-success badge-pill">Heigh</span></td>
                                                    <td>$9.9K</td>
                                                </tr>

                                                <tr>
                                                    <td><img class="round project-thumb" src="img/member-img/team-5.jpg" alt="user">
                                                    <td>
                                                        <h6>Smith</h6><span class="text-muted">Developer</span>
                                                    </td>
                                                    <td>Elite Admin</td>
                                                    <td><span class="badge badge-outline-warning badge-pill">Low</span></td>
                                                    <td>$4.9K</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent user-area d-flex align-items-center justify-content-between">
                                    <h5 class="card-title mb-0">Total Earnings</h5>
                                    <!-- Nav Tabs -->
                                    <ul class="nav total-earnings nav-tabs mb-0" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="today-earnings-tab" data-toggle="tab" href="#today-earnings" role="tab" aria-controls="today-earnings" aria-selected="true">Today</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link mr-0" id="month-earnings-tab" data-toggle="tab" href="#month-earnings" role="tab" aria-controls="month-earnings" aria-selected="false">Month</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="card-body">
                                    <div class="tab-content" id="userList">
                                        <div class="tab-pane fade active show" id="today-earnings" role="tabpanel" aria-labelledby="today-earnings-tab">
                                            <ul class="total-earnings-list">
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nazrul Islam</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$985</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-2.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Ajoy Das</h6>
                                                            <p class="mb-0">Product Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$364</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-3.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Niloy Disk</h6>
                                                            <p class="mb-0">Web Developer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$321</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$1.9k</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-5.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nick Stone</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$879</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-6.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$455</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$1.9k</span>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="tab-pane fade" id="month-earnings" role="tabpanel" aria-labelledby="month-earnings-tab">
                                            <ul class="total-earnings-list">
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-6.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$455</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-2.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Ajoy Das</h6>
                                                            <p class="mb-0">Product Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$364</span>
                                                </li>
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$1.9k</span>
                                                </li>
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-1.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nazrul Islam</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$985</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$1.9k</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-5.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nick Stone</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$879</span>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-7.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-outline-info badge-pill">$1.6k</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Order Statistics</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown81" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown81">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div id="myfirstchart" class="height-300"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Statistics</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown98" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown98">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="row text-center">
                                        <div class="col-12">
                                            <div class="chart mb-15">
                                                <div><span id="sparkline7">7,8,10,7,5,9,10,6,9,4,7,5,9,10,8</span></div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="az-content-label">This Year's Total Revenue</div>
                                            <h5>$ <span class="counter">155,869.78</span></h5>
                                            <p class="mb-0">Sales Performance for Online and Offline Revenue</p>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col-12">
                                            <div class="chart mt-5 mb-15">
                                                <div class=""><span id="sparkline8">3,4,4,7,5,9,10,6,4,4,7,5,9,10,8</span></div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="az-content-label">This Month Income</div>
                                            <h5>$ <span class="counter">83,320.50</span></h5>
                                            <p class="mb-0"><span class="success-text">0.7%</span> higher vs previous month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent user-area d-flex align-items-center justify-content-between">
                                    <h5 class="card-title mb-0">New Users</h5>
                                    <!-- Nav Tabs -->
                                    <ul class="nav total-earnings nav-tabs mb-0" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="today-users-tab" data-toggle="tab" href="#today-users" role="tab" aria-controls="today-users" aria-selected="true">Today</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link mr-0" id="month-users-tab" data-toggle="tab" href="#month-users" role="tab" aria-controls="month-users" aria-selected="false">Month</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="card-body">
                                    <div class="tab-content" id="userList2">
                                        <div class="tab-pane fade active show" id="today-users" role="tabpanel" aria-labelledby="today-users-tab">
                                            <ul class="total-earnings-list">
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-2.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Ajoy Das</h6>
                                                            <p class="mb-0">Product Designer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-primary">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-3.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Niloy Disk</h6>
                                                            <p class="mb-0">Web Developer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-success">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-info">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-5.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nick Stone</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-warning">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-6.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-danger">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-7.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-secondary">Follow</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="tab-pane fade" id="month-users" role="tabpanel" aria-labelledby="month-users-tab">
                                            <ul class="total-earnings-list">
                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-6.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-info">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-2.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Ajoy Das</h6>
                                                            <p class="mb-0">Product Designer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-primary">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-3.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Niloy Disk</h6>
                                                            <p class="mb-0">Web Developer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-success">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-1.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nazrul Islam</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-danger">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-4.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Wiltor Delton</h6>
                                                            <p class="mb-0">Project Manager</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-warning">Follow</a>
                                                </li>

                                                <li>
                                                    <div class="author-info d-flex align-items-center">
                                                        <div class="author-img mr-3">
                                                            <img src="img/member-img/team-5.jpg" alt="">
                                                        </div>
                                                        <div class="author-text">
                                                            <h6 class="mb-0">Nick Stone</h6>
                                                            <p class="mb-0">Visual Designer</p>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="badge badge-success">Follow</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-xl-3 mb-3">
                            <div class="widget-social-content p-3 primary-color full-height">
                                <h6 class="text-white">Facebook User</h6>
                                <h5 class="text-white mb-0">17989</h5>
                                <!-- Widget Social Icon -->
                                <div class="widget-social-icon">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-xl-3 mb-3">
                            <div class="widget-social-content p-3 bg-primary full-height">
                                <h6 class="text-white">Twitter User</h6>
                                <h5 class="text-white mb-0">12978</h5>
                                <!-- Widget Social Icon -->
                                <div class="widget-social-icon">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-xl-3 mb-3">
                            <div class="widget-social-content p-3 bg-danger full-height">
                                <h6 class="text-white">Pinterest User</h6>
                                <h5 class="text-white mb-0">36915</h5>
                                <!-- Widget Social Icon -->
                                <div class="widget-social-icon">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-xl-3 mb-3">
                            <div class="widget-social-content p-3 primary-color full-height">
                                <h6 class="text-white">Instagram User</h6>
                                <h5 class="text-white mb-0">63784</h5>
                                <!-- Widget Social Icon -->
                                <div class="widget-social-icon">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent user-area d-flex align-items-center justify-content-between">
                                    <h6 class="card-title mb-0">Support Tickets</h6>
                                    <ul class="nav nav-tabs mb-0" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="sell-03-tab" data-toggle="tab" href="#sell-03" role="tab" aria-controls="sell-03" aria-selected="true">Today</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link mr-0" id="rent-04-tab" data-toggle="tab" href="#rent-04" role="tab" aria-controls="rent-04" aria-selected="false">Week</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="card-body">
                                    <div class="tab-content" id="ticketList">
                                        <div class="tab-pane fade active show" id="sell-03" role="tabpanel" aria-labelledby="sell-03-tab">
                                            <ul class="ticket-data-list">
                                                <li>
                                                    <div class="avatar-area d-flex">
                                                        <span class="avatar avatar-pending primary-color mr-3">L</span>
                                                        <div class="avatar-text">
                                                            <div class="d-flex align-items-center justify-content-between">
                                                                <h6>Lim Sarah</h6>
                                                                <small class="ticket-time text-muted">9:40 PM</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi eos recusandae obcaecati repudiandae harum quae reprehenderit dicta reiciendis vero nulla dolores velit odit dignissimos minus architecto autem est soluta saepe repellendus, alias voluptatum repellat in voluptate!</p>
                                                            <span class="badge badge-pill badge-primary">Open</span>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="avatar-area d-flex">
                                                        <span class="avatar primary-color mr-3">R</span>
                                                        <div class="avatar-text">
                                                            <div class="d-flex align-items-center justify-content-between">
                                                                <h6>Ritu Jitu</h6>
                                                                <small class="ticket-time text-muted">03.57 PM</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore aliquam cum similique placeat distinctio earum omnis ducimus minus nisi hic praesentium, vitae ea optio eos perspiciatis maiores aut, quae repellat tenetur vero quidem voluptate rem ratione.</p>
                                                            <span class="badge badge-pill badge-secondary">Closed</span>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="avatar-area d-flex">
                                                        <span class="avatar avatar-online primary-color mr-3">A</span>
                                                        <div class="avatar-text">
                                                            <div class="d-flex align-items-center justify-content-between">
                                                                <h6>Ajoy Das</h6>
                                                                <small class="ticket-time text-muted">11.31 AM</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi sequi optio fuga dolorem, aut similique tempore voluptas. Quasi quod, ut neque, possimus, ipsum consectetur dolor quibusdam itaque enim atque aliquam!</p>
                                                            <span class="badge badge-pill badge-warning">Pending</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="tab-pane fade" id="rent-04" role="tabpanel" aria-labelledby="rent-04-tab">
                                            <ul class="ticket-data-list">
                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div class="avatar-area d-flex mr-3">
                                                        <span class="avatar primary-color mr-3">R</span>
                                                        <div class="avatar-text">
                                                            <h6>Ritu Jitu</h6>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore aliquam cum similique placeat distinctio earum omnis ducimus minus nisi hic praesentium, vitae ea optio eos perspiciatis maiores aut, quae repellat tenetur vero quidem voluptate rem ratione.</p>
                                                            <span class="badge badge-pill badge-secondary">Closed</span>
                                                        </div>
                                                    </div>
                                                    <small class="ticket-time text-muted">1 day ago</small>
                                                </li>

                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div class="avatar-area d-flex mr-3">
                                                        <span class="avatar avatar-pending primary-color mr-3">L</span>
                                                        <div class="avatar-text">
                                                            <h6>Lim Sarah</h6>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam tempore vitae fugit corporis a, delectus neque harum perspiciatis, fugiat voluptate, possimus!</p>
                                                            <span class="badge badge-pill badge-primary">Open</span>
                                                        </div>
                                                    </div>
                                                    <small class="ticket-time text-muted">3 day ago</small>
                                                </li>

                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div class="avatar-area d-flex mr-3">
                                                        <span class="avatar avatar-online primary-color mr-3">A</span>
                                                        <div class="avatar-text">
                                                            <h6>Ajoy Das</h6>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores adipisci rerum cum nostrum ex dignissimos labore quas sequi, dicta repudiandae dolor praesentium. Voluptatem fugiat velit nam? Veniam nulla error illo illum.</p>
                                                            <span class="badge badge-pill badge-warning">Pending</span>
                                                        </div>
                                                    </div>
                                                    <small class="ticket-time text-muted">7 day ago</small>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Recent Comments</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown56" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown56">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="comment-widgets-area">
                                        <div class="widgets-single-comment d-flex">
                                            <div><img class="comment-auth-thumb mr-3" src="img/member-img/team-2.jpg" alt=""></div>
                                            <div class="comment-text">
                                                <div class="d-flex justify-content-between">
                                                    <h5>Jhon Anderson</h5>
                                                    <span class="comment-date">Jun 14, 2019</span>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                                <div class="comment-footer d-flex align-items-center justify-content-between mt-15">
                                                    <div class="action-icons">
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-pencil-alt"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Approve"><i class="ti-check"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i></a>
                                                    </div>
                                                    <span class="badge badge-primary">Pending</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="widgets-single-comment d-flex">
                                            <div><img class="comment-auth-thumb mr-3" src="img/member-img/team-3.jpg" alt=""></div>
                                            <div class="comment-text">
                                                <div class="d-flex justify-content-between">
                                                    <h5>Insan Islam</h5>
                                                    <span class="comment-date">Mar 14, 2019</span>
                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates, debitis. Quaerat, laboriosam. Earum quia, quibusdam autem qui dolorem architecto eaque nobis nulla quaerat necessitatibus. quibusdam autem qui dolorem!</p>
                                                <div class="comment-footer d-flex align-items-center justify-content-between mt-15">
                                                    <div class="action-icons">
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-pencil-alt"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Approve"><i class="ti-check"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i></a>
                                                    </div>
                                                    <span class="badge badge-success">Approved</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="widgets-single-comment d-flex">
                                            <div><img class="comment-auth-thumb mr-3" src="img/member-img/team-4.jpg" alt=""></div>
                                            <div class="comment-text">
                                                <div class="d-flex justify-content-between">
                                                    <h5>Lara Smith</h5>
                                                    <span class="comment-date">Jun 14, 2019</span>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos tempora inventore, veniam, consequuntur aspernatur aperiam.</p>
                                                <div class="comment-footer d-flex align-items-center justify-content-between mt-15">
                                                    <div class="action-icons">
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-pencil-alt"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Approve"><i class="ti-check"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i></a>
                                                    </div>
                                                    <span class="badge badge-danger">Rejected</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Sales Data</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown819" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown819">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="c3-chart-area">
                                        <div class="c3-chart">
                                            <div id="bar_stacked_charts"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider--area p-3 bg-white">
                                <div class="widget-slider-3 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-facebook"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-25 bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-facebook"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-25 bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider--area p-3 bg-white">
                                <div class="widget-slider-3 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-instagram"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-50 bg-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-instagram"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-50 bg-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider--area p-3 bg-white">
                                <div class="widget-slider-3 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-dribbble"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-75 bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-dribbble"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-75 bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3 mb-3">
                            <div class="card widget-slider--area p-3 bg-white">
                                <div class="widget-slider-3 owl-carousel">
                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-twitter"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-100 bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Widget Slider Content -->
                                    <div class="widget-slider-3-area">
                                        <div class="slider-icon-text-area d-flex justify-content-between align-items-center">
                                            <div class="icon">
                                                <i class="ti-twitter"></i>
                                            </div>
                                            <div class="text text-right">
                                                <h5 class="mb-0">14,281</h5>
                                                <p class="mb-0 text-success"><i class="ti-arrow-up"></i> +5.79%</p>
                                            </div>
                                        </div>
                                        <div class="slider-content-text">
                                            <h6>Target: <span>35689</span></h6>
                                            <div class="progress h-5">
                                                <div class="progress-bar w-100 bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Browser Stats</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown57" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown57">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/crome.png" alt="">
                                            <h6 class="mb-0">Google Chrome</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-primary">35%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/firefox.png" alt="">
                                            <h6 class="mb-0">Mozila Firefox</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-warning">30%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/safari.png" alt="">
                                            <h6 class="mb-0">Safari</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-info">15%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/ie.png" alt="">
                                            <h6 class="mb-0">Internet Explorer</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-primary">8%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/edge.png" alt="">
                                            <h6 class="mb-0">Edge</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-success">5%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between mb-4">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/opera.png" alt="">
                                            <h6 class="mb-0">Opera Mini</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-success">4%</span>
                                    </div>

                                    <div class="single-browser-area d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center mr-3">
                                            <img class="mr-3" src="img/core-img/uc.png" alt="">
                                            <h6 class="mb-0">UC Browser</h6>
                                        </div>
                                        <span class="badge badge-pill badge-outline-info">3%</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 mb-3">
                            <div class="card full-height">
                                <div class="card-header bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Recent Feeds</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown51" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown51">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="notifications-box">
                                                <a href="#" class="nav-link border-0 px-0 pt-0"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0"><i class="ti-bell bg-danger"></i><span>Domain names expiring</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0"><i class="ti-check"></i><span>Your commissions has been sent</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0"><i class="ti-heart bg-success"></i><span>You sold an item!</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0"><i class="ti-bolt bg-warning"></i><span>Security alert!</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0"><i class="ti-face-smile bg-success"></i><span>We've got something for you!</span> <i class="fa fa-angle-right mr-0"></i></a>
                                                <a href="#" class="nav-link border-0 px-0 pb-0"><i class="ti-bell bg-danger"></i><span>Domain names expiring.</span> <i class="fa fa-angle-right mr-0"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="card code-table full-height">
                                <div class="card-header border-0 bg-transparent d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                        <h5 class="card-title mb-0">Product Status</h5>
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown60" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown60">
                                                <a class="dropdown-item" href="#"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                                                <a class="dropdown-item" href="#"><i class="ti-eraser"></i> Remove</a>
                                                <a class="dropdown-item" href="#"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body pt-0">
                                    <div class="table-responsive">
                                        <table class="table mb-0 border-0 project-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Code</th>
                                                    <th>Date</th>
                                                    <th>Budget</th>
                                                    <th>Status</th>
                                                    <th class="text-right">Ratings</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#471</td>
                                                    <td>8765482</td>
                                                    <td>November 14, 2017</td>
                                                    <td>$874.23</td>
                                                    <td><span class="badge-outline-primary badge-pill">Active</span></td>
                                                    <td class="text-right">
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>#470</td>
                                                    <td>2366482</td>
                                                    <td>November 13, 2017</td>
                                                    <td>$235.34</td>
                                                    <td><span class="badge badge-outline-danger badge-pill">Deactive</span></td>
                                                    <td class="text-right"><a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-o text-warning"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#469</td>
                                                    <td>8765482</td>
                                                    <td>November 14, 2017</td>
                                                    <td>$874.23</td>
                                                    <td><span class="badge-outline-primary badge-pill">Active</span></td>
                                                    <td class="text-right">
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>#468</td>
                                                    <td>2366482</td>
                                                    <td>November 13, 2017</td>
                                                    <td>$235.34</td>
                                                    <td><span class="badge badge-outline-danger badge-pill">Deactive</span></td>
                                                    <td class="text-right"><a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-o text-warning"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#467</td>
                                                    <td>8765482</td>
                                                    <td>November 14, 2017</td>
                                                    <td>$874.23</td>
                                                    <td><span class="badge-outline-primary badge-pill">Active</span></td>
                                                    <td class="text-right">
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>#466</td>
                                                    <td>2366482</td>
                                                    <td>November 13, 2017</td>
                                                    <td>$235.34</td>
                                                    <td><span class="badge badge-outline-danger badge-pill">Deactive</span></td>
                                                    <td class="text-right"><a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-o text-warning"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>#468</td>
                                                    <td>2366482</td>
                                                    <td>November 13, 2017</td>
                                                    <td>$235.34</td>
                                                    <td><span class="badge badge-outline-danger badge-pill">Deactive</span></td>
                                                    <td class="text-right"><a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-o text-warning"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#467</td>
                                                    <td>8765482</td>
                                                    <td>November 14, 2017</td>
                                                    <td>$874.23</td>
                                                    <td><span class="badge-outline-primary badge-pill">Active</span></td>
                                                    <td class="text-right">
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star text-warning"></i></a>
                                                        <a href="#"><i class="fa fa-star-half-o text-warning"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('components.footer')
               
            </div>
        </div>
    </div>
    <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

    @include('components.mainjs')

    <!-- These plugins only need for the run this page -->
    <script src="{{ asset('public/js/d3.min.js')}}"></script>
    <script src="{{ asset('public/js/c3.min.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/c3-chart-script.js')}}"></script>
    <script src="{{ asset('public/js/charts/morris/morris.min.js')}}"></script>
    <script src="{{ asset('public/js/charts/morris/raphael-min.js')}}"></script>
    <script src="{{ asset('public/js/charts/morris/morris.custom.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/sparkline-small.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/sparkline.min.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/echart-basic.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/funnel-script.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/radar-script.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/vector-map/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/vector-map/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/vector-map/jvectormap.custom.js')}}"></script>

</body>

</html>