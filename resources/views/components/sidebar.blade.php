<!-- Sidemenu Area -->
        <div class="ecaps-sidemenu-area">
            <!-- Desktop Logo -->
            <div class="ecaps-logo">
                <a href="index.html"><img class="desktop-logo" src="img/core-img/logo.png" alt="Desktop Logo"> <img class="small-logo" src="img/core-img/small-logo.png" alt="Mobile Logo"></a>
            </div>

            <!-- Side Nav -->
            <div class="ecaps-sidenav" id="ecapsSideNav">
                <!-- Side Menu Area -->
                <div class="side-menu-area">
                    <!-- Sidebar Menu -->
                    <nav>
                        <ul class="sidebar-menu" data-widget="tree">
                            <li class="sidemenu-user-profile d-flex align-items-center">
                                <div class="user-thumbnail">
                                    <img src="img/member-img/team-2.jpg" alt="">
                                </div>
                                <div class="user-content">
                                    <h6>Ajoy Das</h6>
                                    <span>Admin</span>
                                </div>
                            </li>
                            <li class="active">
                                <a href="index.html"><i class="ti-dashboard"></i><span>Dashboard</span></a>
                            </li>
                           
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="calendar.html">Calender</a></li>
                                    <li><a href="chat-box.html">Chat Box</a></li>
                                </ul>
                            </li>
                            <!-- <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-email"></i> <span>Inbox</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="inbox.html">Inbox</a></li>
                                    <li><a href="email-view.html">Email View</a></li>
                                    <li><a href="compose-email.html">Compose Mail</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-palette"></i> <span>UI Elements</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="buttons.html">Button</a></li>
                                    <li><a href="card.html">Card</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="notifications.html">Notification</a></li>
                                    <li><a href="animation.html">Animation</a></li>
                                    <li><a href="agile-board.html">Agile Board</a></li>
                                    <li><a href="badges.html">Badges</a></li>
                                    <li><a href="breadcrumb.html">Breadcrumb</a></li>
                                    <li><a href="timeline.html">Timeline</a></li>
                                    <li><a href="accordian.html">Accordion</a></li>
                                    <li><a href="file-manager.html">File Manager</a></li>
                                    <li><a href="pagination.html">Pagination</a></li>
                                    <li><a href="progressbar.html">Progressbar</a></li>
                                    <li><a href="banner.html">Banner</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-ruler-pencil"></i> <span>Advanced UI</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="nestable-list.html">Nestable List</a></li>
                                    <li><a href="rating.html">Rating</a></li>
                                    <li><a href="todo-list.html">Todo list</a></li>
                                    <li><a href="dragula.html">Dragula</a></li>
                                    <li><a href="modals.html">Modals</a></li>
                                    <li><a href="clipboard.html">Clipboard</a></li>
                                    <li><a href="preloader.html">Loader</a></li>
                                    <li><a href="summernote.html">Summernote</a></li>
                                    <li><a href="sweet-alerts.html">Sweet Alert</a></li>
                                    <li><a href="slider.html">Carousel</a></li>
                                    <li><a href="context-menu.html">Context Menu</a></li>
                                    <li><a href="code-editors.html">Code Editor</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-clipboard"></i> <span>Forms</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="basic-form.html">Basic Form</a></li>
                                    <li><a href="form-picker.html">Form Picker</a></li>
                                    <li><a href="form-wizard.html">Wizard Form</a></li>
                                    <li><a href="form-validation.html">Form Validation</a></li>
                                    <li><a href="form-repeater.html">Form Repeater</a></li>
                                    <li><a href="file-upload.html">File Upload</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-agenda"></i> <span>Tables</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="basic-table.html">Basic Table</a></li>
                                    <li><a href="data-table.html">Data Table</a></li>
                                    <li><a href="bootstrap-table.html">Bootstrap Table</a></li>
                                    <li><a href="edit-table.html">Edit Table</a></li>
                                    <li><a href="js-grid-table.html">Js Grid Table</a></li>
                                    <li><a href="price-table.html">Price Table</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-bar-chart"></i> <span>Charts</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">

                                    <li><a href="morris-chart.html">Morris Chart</a></li>


                                    <li><a href="c3-charts.html">C3 Chart</a></li>
                                    <li><a href="c3-api-chart.html">C3 Api Chart</a></li>
                                    <li><a href="echart-candelstick.html">Echart Candelstick</a></li>
                                    <li><a href="echart-funnel-radar.html">Echart Funnel Radar</a></li>
                                    <li><a href="chart-sparkline.html">Chart Sparkline</a></li>
                                    <li><a href="echart-scatters.html">Echart Scatters</a></li>
                                    <li><a href="e-chart-js.html">Echart</a></li>
                                    <li><a href="google-chart.html">Google Chart</a></li>
                                    <li><a href="peity-chart.html">Peity Chart</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-notepad"></i> <span>Simple Pages</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="team.html">Team</a></li>
                                    <li><a href="gallery.html">Gallery</a></li>
                                    <li><a href="account-settings.html">Account Settings</a></li>
                                    <li><a href="invoice.html">Invoice</a></li>
                                    <li><a href="call-to-action.html">Call To Action</a></li>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="search-result.html">Search Results</a></li>
                                    <li><a href="vote-list.html">Vote List</a></li>
                                    <li><a href="video.html">Video</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="blank.html">Blank Page</a></li>
                                </ul>
                            </li>
                            <li><a href="blog.html"><i class="ti-layout-media-overlay-alt-2"></i> <span>Blog</span></a></li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-lock"></i> <span>User Page</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="login.html">Login</a></li>
                                    <li><a href="register.html">Register</a></li>
                                    <li><a href="lock-screen.html">Lock Screen</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-shopping-cart-full"></i> <span>E-commerce</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="shop.html">Shop</a></li>
                                    <li><a href="product-edit.html">Product Edit</a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-map"></i> <span>Maps</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="maple-map.html">Maple Map</a></li>
                                    <li><a href="google-map.html">Google Map</a></li>
                                    <li><a href="vector-map.html">Vector Map</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-world"></i> <span>Icons</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="flag-icon.html">Flag Icons</a></li>
                                    <li><a href="elegant-icons.html">Elegant Icons</a></li>
                                    <li><a href="et-line-icons.html">Et-line Icons</a></li>
                                    <li><a href="font-awesome.html">Font-Awsome Icons</a></li>
                                    <li><a href="matarial-icons.html">Materialize Icons</a></li>
                                    <li><a href="pe-7-stroke.html">Pe-7 Stroke Icons</a></li>
                                    <li><a href="themify-icons.html">Themify Icons</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="javascript:void(0)"><i class="ti-menu"></i> <span>Multilevel</span> <i class="fa fa-angle-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="#">Level One</a></li>
                                    <li class="treeview">
                                        <a href="#">Level One <i class="fa fa-angle-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="#">Level Two</a></li>
                                            <li><a href="#">Level Two</a></li>
                                            <li><a href="#">Level Two</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Level One</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>