  <!-- Must needed plugins to the run this Template -->
    <script src="{{ asset('public/js/jquery.min.js')}}"></script>
    <script src="{{ asset('public/js/popper.min.js')}}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/js/ecaps.bundle.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/todolist.js')}}"></script>
    <script src="{{ asset('public/js/default-assets/date-time.js')}}"></script>

    <!-- Active JS -->
    <script src="{{ asset('public/js/default-assets/active.js')}}"></script>