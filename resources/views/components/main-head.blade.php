<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Ecaps - Responsive Bootstrap 4 Admin Template</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- These plugins only need for the run this page -->
  
    <link rel="stylesheet" href="{{ asset('public/css/c3.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/css/default-assets/morris.css')}}">
    <link rel="stylesheet" href="{{ asset('public/js/default-assets/vector-map/jquery-jvectormap-2.0.2.css')}}">

    <!-- Master Stylesheet [If you remove this CSS file, your file will be broken undoubtedly.] -->
    <link rel="stylesheet" href="{{ asset('public/style.css')}}">

</head>